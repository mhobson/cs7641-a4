# Project 4: Markov Decision Processes

Matt Hobson  
CS7641 - Spring 2019  
GTID: mhobson7

## Code

Code for this assignment can be found here:

    https://gitlab.com/mhobson/cs7641-a4

This was developed under Python 3.7. You can probably run it with 3.5
or later.

If you are familiar with Pipenv, you can use that to set up your
virtual environment. If not, there's a standard Python
`requirements.txt` file you can use.

## Running the project

1. Run `python3 run_experiment.py --threads -1 --all` to run the
   experiments.
2. Plot the final results via `python3 run_experiment.py --plot`.

You can do it all in one line too:

```sh
python3 run_experiment.py --threads -1 --all --plot
```

## Acknowledgements

The base of the code used for this project is from
[Michael Mallo](https://github.gatech.edu/mmallo3/CS7641_Project4) and
[Chad Maron](https://github.com/cmaron/CS-7641-assignments).
