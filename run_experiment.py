#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Matt Hobson <mhobson7@gatech.edu>
# CS7641 Spring 2019

import argparse
import logging
import random as rand
from datetime import datetime
from pathlib import Path

import numpy as np
from logzero import logger, logfile, loglevel

import environments
import experiments
from experiments import plotting

# Configure rewards per environment
ENV_REWARDS = {
    "fall_rew": -10,
    "goal_rew": 10,
    "hole_rew": -10,
    "step_rew": -0.1,
    "step_prob": 0.8,
    "wind_prob": 0.1,
}

# Configure max steps per experiment
MAX_STEPS = {"pi": 100, "vi": 100, "ql": 100}

# Configure trials per experiment
NUM_TRIALS = {"pi": 100, "vi": 100, "ql": 100}

# Configure thetas per experiment
PI_THETA = VI_THETA = QL_THETA = 0.00001

# Configure discounts per experiment (lists of discount values)
PI_DISCOUNTS = VI_DISCOUNTS = QL_DISCOUNTS = [0.0, 0.1, 0.2, 0.3, 0.5, 0.7, 0.9]

# Configure other QL experiment parameters
QL_MAX_EPISODES: int = 100
QL_MIN_EPISODES: int = max(QL_MAX_EPISODES * 0.01, 5)
QL_MAX_EPISODE_STEPS: int = 100
# Number of consecutive episodes with little change before calling it converged
QL_MIN_SUB_THETAS: int = 5
QL_ALPHAS = [0.1, 0.5, 0.9]
QL_Q_INITS = ["random", 0]
QL_EPSILONS = [0.1, 0.3, 0.5]
QL_EPSILON_DECAYS = [0.0001]

# Check configuration settings (just make sure they've been set to something)
for setting in ENV_REWARDS.keys():
    if ENV_REWARDS[setting] is None:
        logger.error(f"ENV_REWARDS[{setting}] not configured.")
        raise ValueError(f"{setting} not configured.")
for exp in MAX_STEPS.keys():
    if MAX_STEPS[exp] is None:
        logger.error(f"MAX_STEPS[{exp}] not set.")
        raise ValueError(f"MAX_STEPS[{exp}] not set.")
for exp in NUM_TRIALS.keys():
    if NUM_TRIALS[exp] is None:
        logger.error(f"NUM_TRIALS[{exp}] not set.")
        raise ValueError(f"NUM_TRIALS[{exp}] not set.")
if PI_THETA is None or VI_THETA is None or QL_THETA is None:
    logger.error("Not all experiment thetas set.")
    raise ValueError("Not all experiment thetas set.")
if len(PI_DISCOUNTS) == 0 or len(VI_DISCOUNTS) == 0 or len(QL_DISCOUNTS) == 0:
    logger.error("Not all experiment discounts set.")
    raise ValueError("Not all experiment discounts set.")
if (
    QL_MAX_EPISODES is None
    or QL_MIN_EPISODES is None
    or QL_MAX_EPISODE_STEPS is None
    or QL_MIN_SUB_THETAS is None
    or len(QL_ALPHAS) == 0
    or len(QL_Q_INITS) == 0
    or len(QL_EPSILONS) == 0
    or len(QL_EPSILON_DECAYS) == 0
):
    logger.error("Not all QL experiment parameters set.")
    raise ValueError("Not all QL experiment parameters set.")

# Configure logging
loglevel(logging.DEBUG)
program_start = datetime.now().strftime("%Y%m%d%H%M%S")
logfile(filename=f"{Path(__file__).stem}-{program_start}.log", loglevel=logging.DEBUG)
logger.info(f"Program start time: {program_start}")


def run_experiment(
    experiment_details,
    experiment,
    timing_key,
    verbose,
    timings,
    max_steps,
    num_trials,
    theta=None,
    max_episodes=None,
    min_episodes=None,
    max_episode_steps=None,
    min_sub_thetas=None,
    discounts=None,
    alphas=None,
    q_inits=None,
    epsilons=None,
    epsilon_decays=None,
):
    timings[timing_key] = {}
    for details in experiment_details:
        t = datetime.now()
        logger.info(
            "Running {} experiment: {}".format(timing_key, details.env_readable_name)
        )
        if timing_key == "QL":  # Q-Learning
            exp = experiment(
                details,
                verbose=verbose,
                max_steps=max_steps,
                num_trials=num_trials,
                max_episodes=max_episodes,
                min_episodes=min_episodes,
                max_episode_steps=max_episode_steps,
                min_sub_thetas=min_sub_thetas,
                theta=theta,
                discounts=discounts,
                alphas=alphas,
                q_inits=q_inits,
                epsilons=epsilons,
                epsilon_decays=epsilon_decays,
            )
        else:  # NOT Q-Learning
            exp = experiment(
                details,
                verbose=verbose,
                max_steps=max_steps,
                num_trials=num_trials,
                theta=theta,
                discounts=discounts,
            )
        exp.perform()
        t_d = datetime.now() - t
        timings[timing_key][details.env_name] = t_d.seconds


if __name__ == "__main__":

    # Parse arguments
    parser = argparse.ArgumentParser(description="Run MDP experiments")
    parser.add_argument(
        "--threads",
        type=int,
        default=-1,
        help="Number of threads (defaults to -1 for auto)",
    )
    parser.add_argument("--seed", type=int, help="A random seed to set, if desired")
    parser.add_argument(
        "--policy", action="store_true", help="Run the Policy Iteration (PI) experiment"
    )
    parser.add_argument(
        "--value", action="store_true", help="Run the Value Iteration (VI) experiment"
    )
    parser.add_argument(
        "--ql", action="store_true", help="Run the Q-Learner (QL) experiment"
    )
    parser.add_argument("--all", action="store_true", help="Run all experiments")
    parser.add_argument("--plot", action="store_true", help="Plot data results")
    parser.add_argument(
        "--verbose", action="store_true", help="If true, provide verbose output"
    )
    args = parser.parse_args()
    verbose = args.verbose
    threads = args.threads

    # Set random seed
    seed = args.seed
    if seed is None:
        seed = np.random.randint(0, (2 ** 32) - 1)
    logger.info(f"Using seed {seed}")
    np.random.seed(seed)
    rand.seed(seed)

    logger.info("Creating MDPs.")

    # Modify this list of dicts to add/remove/swap environments
    envs = [
        {
            "env": environments.get_windy_cliff_walking_4x12_environment(
                wind_prob=ENV_REWARDS["wind_prob"],
                step_rew=ENV_REWARDS["step_rew"],
                fall_rew=ENV_REWARDS["fall_rew"],
                goal_rew=ENV_REWARDS["goal_rew"],
            ),
            "name": "cliff_walking",
            "readable_name": "Cliff Walking (4x12)",
        },
        {
            "env": environments.get_large_rewarding_frozen_lake_20x20_environment(
                step_prob=ENV_REWARDS["step_prob"],
                step_rew=ENV_REWARDS["step_rew"],
                hole_rew=ENV_REWARDS["hole_rew"],
                goal_rew=ENV_REWARDS["goal_rew"],
            ),
            "name": "large_frozen_lake_20x20",
            "readable_name": "Frozen Lake (20x20)",
        },
        {
            "env": environments.get_large_rewarding_frozen_lake_15x15_environment(
                step_prob=ENV_REWARDS["step_prob"],
                step_rew=ENV_REWARDS["step_rew"],
                hole_rew=ENV_REWARDS["hole_rew"],
                goal_rew=ENV_REWARDS["goal_rew"],
            ),
            "name": "large_frozen_lake_15x15",
            "readable_name": "Frozen Lake (15x15)",
        },
        {
            "env": environments.get_large_rewarding_frozen_lake_12x12_environment(
                step_prob=ENV_REWARDS["step_prob"],
                step_rew=ENV_REWARDS["step_rew"],
                hole_rew=ENV_REWARDS["hole_rew"],
                goal_rew=ENV_REWARDS["goal_rew"],
            ),
            "name": "large_frozen_lake_12x12",
            "readable_name": "Frozen Lake (12x12)",
        },
        {
            "env": environments.get_rewarding_frozen_lake_8x8_environment(
                step_prob=ENV_REWARDS["step_prob"],
                step_rew=ENV_REWARDS["step_rew"],
                hole_rew=ENV_REWARDS["hole_rew"],
                goal_rew=ENV_REWARDS["goal_rew"],
            ),
            "name": "frozen_lake_8x8",
            "readable_name": "Frozen Lake (8x8)",
        },
        {
            "env": environments.get_rewarding_frozen_lake_4x4_environment(
                step_prob=ENV_REWARDS["step_prob"],
                step_rew=ENV_REWARDS["step_rew"],
                hole_rew=ENV_REWARDS["hole_rew"],
                goal_rew=ENV_REWARDS["goal_rew"],
            ),
            "name": "frozen_lake_4x4",
            "readable_name": "Frozen Lake (4x4)",
        },
    ]
    # Set up experiments
    experiment_details = []
    for env in envs:
        env["env"].seed(seed)
        logger.info(
            "{}: State space: {}, Action space: {}".format(
                env["readable_name"], env["env"].unwrapped.nS, env["env"].unwrapped.nA
            )
        )
        experiment_details.append(
            experiments.ExperimentDetails(
                env["env"],
                env["name"],
                env["readable_name"],
                threads=threads,
                seed=seed,
            )
        )
    logger.info("Running experiments")
    # Dict used to report experiment times (in seconds) at the end of the run
    timings = {}
    # Run Policy Iteration (PI) experiment
    if args.policy or args.all:
        run_experiment(
            experiment_details,
            experiments.PolicyIterationExperiment,
            "PI",
            verbose,
            timings,
            MAX_STEPS["pi"],
            NUM_TRIALS["pi"],
            theta=PI_THETA,
            discounts=PI_DISCOUNTS,
        )
    # Run Value Iteration (VI) experiment
    if args.value or args.all:
        run_experiment(
            experiment_details,
            experiments.ValueIterationExperiment,
            "VI",
            verbose,
            timings,
            MAX_STEPS["vi"],
            NUM_TRIALS["vi"],
            theta=VI_THETA,
            discounts=VI_DISCOUNTS,
        )
    # Run Q-Learning (QL) experiment
    if args.ql or args.all:
        run_experiment(
            experiment_details,
            experiments.QLearnerExperiment,
            "QL",
            verbose,
            timings,
            MAX_STEPS["ql"],
            NUM_TRIALS["ql"],
            max_episodes=QL_MAX_EPISODES,
            max_episode_steps=QL_MAX_EPISODE_STEPS,
            min_episodes=QL_MIN_EPISODES,
            min_sub_thetas=QL_MIN_SUB_THETAS,
            theta=QL_THETA,
            discounts=QL_DISCOUNTS,
            alphas=QL_ALPHAS,
            q_inits=QL_Q_INITS,
            epsilons=QL_EPSILONS,
            epsilon_decays=QL_EPSILON_DECAYS,
        )
    # Generate plots
    if args.plot:
        logger.info("Plotting results")
        plotting.plot_results(envs)

    # Output timing information
    if len(timings) > 0:
        logger.info(f"Timings: {timings}")
    else:
        logger.warning("No timings to report.")
